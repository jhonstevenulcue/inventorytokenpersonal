package com.adsi.inventory.web.rest;

import com.adsi.inventory.service.IUserService;
import com.adsi.inventory.service.dto.UsersDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class UsersResource {

    @Autowired
    IUserService userService;

    @GetMapping("/user")
    public Page<UsersDTO> getAll(@RequestParam(value = "pageNumber") Integer pageNumber,
                                 @RequestParam(value = "pageSize") Integer pageSize){
        return userService.getAll(pageNumber, pageSize);
    }

    @PostMapping("/user")
    public ResponseEntity<?> create(@Valid @RequestBody UsersDTO usersDTO, BindingResult result){
        UsersDTO dto = null;
        Map<String, Object> response = new HashMap<>();
        if(result.hasErrors()){
            List<String> errors = result.getFieldErrors()
                    .stream()
                    .map(e -> "Error en el campo " + e.getField() + ": " + e.getDefaultMessage())
                    .collect(Collectors.toList());
            response.put("error", errors);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
        try{
            dto = userService.create(usersDTO);
        } catch (DataAccessException err){
            response.put("message", "Error al crear un usuario en la base de datos");
            response.put("error", err.getMessage() + ": " +
                    err.getMostSpecificCause().getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        response.put("message", "El usuario ha sido creado correctamente");
        response.put("user", dto);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @GetMapping("/user/{id}")
    public ResponseEntity<UsersDTO> getById(@PathVariable Long id){
        return new ResponseEntity<>(userService.getById(id), HttpStatus.OK);
    }
}
