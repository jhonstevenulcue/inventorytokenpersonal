package com.adsi.inventory.web.rest;

import com.adsi.inventory.service.IProductService;
import com.adsi.inventory.service.dto.ProductDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class ProductResourse {

    @Autowired
    IProductService service;

    @GetMapping("/product")
    public Page<ProductDTO> getAll(@RequestParam(value = "page") Integer pageNumber,
                                   @RequestParam(value = "size") Integer pageSize){
        return service.getAll(pageNumber, pageSize);
    }

    @PostMapping("/product")
    public ResponseEntity<?> create(@Valid @RequestBody ProductDTO productDTO , BindingResult result){
        ProductDTO dto = null;
        Map<String, Object> response = new HashMap<>();
        if(result.hasErrors()){
            List<String> errors = result.getFieldErrors()
                    .stream()
                    .map(e -> "Error en el campo " + e.getField() + ": " + e.getDefaultMessage())
                    .collect(Collectors.toList());
            response.put("error", errors);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
        try{
            dto = service.create(productDTO);
        } catch (DataAccessException err){
            response.put("message", "Error al crear un producto en la base de datos");
            response.put("error", err.getMessage() + ": " +
                    err.getMostSpecificCause().getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        response.put("message", "El producto ha sido creado correctamente");
        response.put("product", dto);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @GetMapping("/product/{reference}")
    public ResponseEntity<ProductDTO> getById(@PathVariable String reference){
        return new ResponseEntity<>(service.getById(reference), HttpStatus.OK);
    }
}
