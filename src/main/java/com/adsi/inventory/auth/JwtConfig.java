package com.adsi.inventory.auth;


public class JwtConfig {

    public static final String RSA_PRIVATE = "-----BEGIN RSA PRIVATE KEY-----\n" +
            "MIIEowIBAAKCAQEAs8TmEm1wCB9mJJOJkMbv7fs+2W7esTBm5VcVILUOkg/KsWDq\n" +
            "uVeBBuiD3BpvXidfIoiwGUCBQoCaJuXxaARpjCh4ehGOrzVxyKTkUqMdR4LaV9nY\n" +
            "sWNSiCe8P4Bd7dOBqT3rsRWKOsHPtu3EosMCxfYGI3QGGylsY0wEwMjeYyXJWc3p\n" +
            "PzTgYZOOgoLO/DopahazFpL62Ko1blblcDIIGlZJ8MrfpbIHhsHFuZEo/kicA9ky\n" +
            "K9UliBGx0tgiyQECCJtZDYbpPdRWrr8IrxMquN3T38n8PakdJL2s1BkR763eH4Ii\n" +
            "eRRcn/uGrevj3Q2G2KQRz9YuGSGsRBQMbtpd+wIDAQABAoIBAEd/3sohgNeaNXQC\n" +
            "wko09aS/syWj9SqgR6yb0ClsGdl6MnIOTA1fglAPFrLqSLU4D5yapP/GKiMbJzSl\n" +
            "bDik2J4IUrKqXQfOx+y3uKAthC23G8SpPoh/KopA05ymH7XB0+ZOXHnYSVxJTaZC\n" +
            "N11NFsNJsPNU6EDRXuy3hYRv4BOqfx8RwDJrFPhOIj4yFShGmAC+mRGG9GWNKjHb\n" +
            "dHvIw3hp/S0U3+bG3ijfOFmcHwPSqXCsZTxnA351R/+v18W32bs0otUAb/CJhu9+\n" +
            "J+CcKTgHRJOtqQkkPnoekgnoo/R/7/MUCUgxkawib1COQZegqXP8uQDzq3GN3x/f\n" +
            "XVFRhDECgYEA1uZqJA9TqrH6PgUpmgpeb4BedlhrKeYhZ0l1IL1Jhb8Nl3eqAzUo\n" +
            "Vvs4YCRbD5DHhsgTgY6OBv4jYGS51iJphUNmxuCTDsP278raKvVMx3iIujdlzYVc\n" +
            "bcycdtFZ939b5Qi+A2xQ9X4sZ11MPA9xjbVTANLRxKrGph571yRQkDUCgYEA1iZ2\n" +
            "Gpc5yJQCkqlsRC8MBjXyNDd8l/+9CXlO/ySCBbGtQDjgnuIzfUfk6ym5JciM1hvz\n" +
            "/O/kOHjVmniM0ClG0j5vV0B29R8LHXTsURkPYXZEC21nGiqB/494y4tBPF6kn76/\n" +
            "mZjJMt3OTYEV9eIN96OFbGNJ8TUQtcC5zgElW28CgYEAzoWk336zQ0nDWbMQZe8t\n" +
            "Di7pmQYkBqIY2/AmVg5TB+uxnizsACUn3V+UsCCAOpdCiO7igBGFwIafbNWgGn/7\n" +
            "tIwvy1SrPvdo6s2hmMMgxFm73Gm8tiRBzWtjunQ657VHlPYv1Y5Ux3g4KCXrS5Tz\n" +
            "5eGqIRJ5cW1FdwO4s+s8WTkCgYAqhzLXLwcbrUEhjSHiHJG/vgDem9JTgUcyVXP5\n" +
            "uW9U4Ccu5Ny+0N821wtAJGYPedd2QdDff2iMSnrpFV2pjTjyUdHcuwGkxkh3zhuM\n" +
            "M3TZfxoeRti92ok8jmqHyZzn90esLb5xK2Vxg5kPzT3mOcFaV5AMqV8q1sX9wktj\n" +
            "/NhFYwKBgBPYDOjY5cOQ9wBmjahJxljebmQVx17kGwVqxas3r7dwKILnlrlwgLrL\n" +
            "IzntC3IbDF+uhEczv2tt7Xcpdlb3QIRTMEsMfIjkr0seHbR+hd/fwvvDgJTbr70N\n" +
            "+dCID93F6TzMvpCVq3ki9wMs6ECqYyKTNGwVB7adBNyVXrBuf4xp\n" +
            "-----END RSA PRIVATE KEY-----";

    public static final String RSA_PUBLIC ="-----BEGIN PUBLIC KEY-----\n" +
            "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAs8TmEm1wCB9mJJOJkMbv\n" +
            "7fs+2W7esTBm5VcVILUOkg/KsWDquVeBBuiD3BpvXidfIoiwGUCBQoCaJuXxaARp\n" +
            "jCh4ehGOrzVxyKTkUqMdR4LaV9nYsWNSiCe8P4Bd7dOBqT3rsRWKOsHPtu3EosMC\n" +
            "xfYGI3QGGylsY0wEwMjeYyXJWc3pPzTgYZOOgoLO/DopahazFpL62Ko1blblcDII\n" +
            "GlZJ8MrfpbIHhsHFuZEo/kicA9kyK9UliBGx0tgiyQECCJtZDYbpPdRWrr8IrxMq\n" +
            "uN3T38n8PakdJL2s1BkR763eH4IieRRcn/uGrevj3Q2G2KQRz9YuGSGsRBQMbtpd\n" +
            "+wIDAQAB\n" +
            "-----END PUBLIC KEY-----";
}
