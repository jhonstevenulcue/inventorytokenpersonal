package com.adsi.inventory.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Users implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty(message = "Este campo no puede estar vacio")
    @Size(min = 4, max = 20, message = "El tamaño del campo debe ser entre 4 y 20 caracteres")
    @Column(unique = true, length = 20)
    private String username;

    @NotEmpty(message = "Este campo no puede estar vacio")
    @Size(min = 2, max = 40, message = "El tamaño del campo debe ser entre 4 y 20 caracteres")
    @Column(length = 40)
    private String name;

    @NotEmpty(message = "Este campo no puede estar vacio")
    @Size(min = 2, max = 40, message = "El tamaño del campo debe ser entre 4 y 20 caracteres")
    @Column(length = 40)
    private String lastName;

    @NotEmpty(message = "Este campo no puede estar vacio")
    @Email(message = "El email no tiene la estrcutura correcta")
    @Column(unique = true)
    private String email;

    @NotEmpty(message = "Este campo no puede estar vacio")
    @Column(length = 60)
    private String password;

    private Boolean enabled;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "user_has_rol", joinColumns = @JoinColumn(name = "user_id"),
    inverseJoinColumns = @JoinColumn(name = "rol_id"),
    uniqueConstraints = {@UniqueConstraint(columnNames = {"user_id", "rol_id"})})
    private List<Rols> rols;
}
