package com.adsi.inventory.service.Imp;

import com.adsi.inventory.domain.Users;
import com.adsi.inventory.repository.UserRepository;
import com.adsi.inventory.service.IUserService;
import com.adsi.inventory.service.dto.UsersDTO;
import com.adsi.inventory.service.error.ObjectNotFoundException;
import com.adsi.inventory.service.transformer.UsersTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class IUserServiceImp implements IUserService {

    @Autowired
    UserRepository repository;

    @Override
    public Page<UsersDTO> getAll(Integer pageNumber, Integer pageSize) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        return repository.findAll(pageable)
                .map(UsersTransformer::getUsersDTOFromUsers);
    }

    @Override
    public UsersDTO create(UsersDTO usersDTO) {
        return UsersTransformer.getUsersDTOFromUsers(repository.save(UsersTransformer.getUsersFromUsersDTO(usersDTO)));
    }

    @Override
    public UsersDTO getById(Long id) {
        Optional<Users> user = repository.findById(id);
        if(!user.isPresent()) {
            throw new ObjectNotFoundException("error: el usuario con id = " + id + " no existe");
        }
        return user
                .map(UsersTransformer::getUsersDTOFromUsers)
                .get();
    }
}
