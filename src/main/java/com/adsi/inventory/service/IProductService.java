package com.adsi.inventory.service;

import com.adsi.inventory.domain.Product;
import com.adsi.inventory.service.dto.ProductDTO;
import org.springframework.data.domain.Page;

public interface IProductService {
    Page<ProductDTO> getAll(Integer pageNumber, Integer pageSize);

    ProductDTO create(ProductDTO productDTO);
    ProductDTO getById(String reference);
}
