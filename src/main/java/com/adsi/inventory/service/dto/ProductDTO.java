package com.adsi.inventory.service.dto;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Getter
@Setter
public class ProductDTO implements Serializable {
    @Id
    private String reference;

    @NotEmpty(message = "Este campo no puede estar vacio")
    @Size(min = 2, max = 20, message = "El tamaño deber ser entre 2 y 20 caracteres")
    private String name;
}
