package com.adsi.inventory.service.transformer;

import com.adsi.inventory.domain.Users;
import com.adsi.inventory.service.dto.UsersDTO;

public class UsersTransformer {

    public static UsersDTO getUsersDTOFromUsers(Users users){
        if (users == null){
            return null;
        }

        UsersDTO dto = new UsersDTO();

        dto.setId(users.getId());
        dto.setName(users.getName());
        dto.setLastName(users.getLastName());
        dto.setUsername(users.getUsername());
        dto.setPassword(users.getPassword());
        dto.setEmail(users.getEmail());
        dto.setRols(users.getRols());
        return dto;
    }


    public static Users getUsersFromUsersDTO(UsersDTO dto){
        if (dto == null){
            return null;
        }

        Users users = new Users();

        users.setId(dto.getId());
        users.setName(dto.getName());
        users.setLastName(dto.getLastName());
        users.setUsername(dto.getUsername());
        users.setPassword(dto.getPassword());
        users.setEmail(dto.getEmail());
        users.setRols(dto.getRols());
        return users;
    }
}
