package com.adsi.inventory.service.transformer;

import com.adsi.inventory.domain.Product;
import com.adsi.inventory.service.dto.ProductDTO;

public class ProductTransformer {

    public static ProductDTO getProductDTOFromProduct(Product product){
        if(product == null){
            return null;
        }

        ProductDTO dto = new ProductDTO();
        dto.setReference(product.getReference());
        dto.setName(product.getName());
        return dto;
    }

    public static Product getProductFromProductDTO(ProductDTO dto){
        if(dto == null){
            return null;
        }

        Product users = new Product();
        users.setReference(dto.getReference());
        users.setName(dto.getName());
        return users;
    }
}
