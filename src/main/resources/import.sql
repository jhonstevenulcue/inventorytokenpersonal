INSERT INTO users(email, last_name, name, password, username, enabled) VALUES ('jhonulcue@gmail.com','Jhon', 'Steven', '$2a$10$DycFlWggW71frFg5SIiQDOz9TWgbpbAhdcuhoE9uXwmJAan8PFZIK', 'jhon', true);
INSERT INTO users(email, last_name, name, password, username, enabled) VALUES ('yeisonarley@gmail.com','Yeison', 'Arley', '$2a$10$qJSyuUKfj1baJ8HLtI73fuFY1VFK1ADDKCBm8TBdsU92imjyL9YVO', 'yeison', true );
INSERT INTO users(email, last_name, name, password, username, enabled) VALUES ('sebastianrojas@gmail.com','Sebastian', 'Rojas', '$2a$10$qJSyuUKfj1baJ8HLtI73fuFY1VFK1ADDKCBm8TBdsU92imjyL9YVO', 'sebastian', true);
INSERT INTO users(email, last_name, name, password, username, enabled) VALUES ('camiloordoñes@gmail.com','Camilo', 'Steven', '$2a$10$qJSyuUKfj1baJ8HLtI73fuFY1VFK1ADDKCBm8TBdsU92imjyL9YVO', 'Ordoñes', true);

INSERT INTO rols VALUES (1, 'ROLE_USER');
INSERT INTO rolS VALUES (2, 'ROLE_ADMIN');

INSERT INTO user_has_rol VALUES (1,1), (1,2);
INSERT INTO user_has_rol VALUES (2,1);
INSERT INTO user_has_rol VALUES (3,2);

INSERT INTO product(reference, name) VALUES ('1', 'Teclado'), ('2', 'Portatil');